# DevOps-DF Rancher Catalog

Este é um repositório de templates criados pela comunidade DevOps-DF, ele é um catálogo de aplicações a serem provisionadas em Rancher. Porém, não é mantido ou suportado pela Rancher Labs.


## License

Copyright [2016] [DevOps-DF]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

For details, see:
[/LICENSES/LICENSE](/LICENSES/LICENSE)
